import { createSlice } from '@reduxjs/toolkit'

export const basketSlice = createSlice({
  name: 'basket',
  initialState: {
    basketList: []
  },
  reducers: {
    addProductToBasket: (state, action) => {
      state.basketList.push(
        {
          id: action.payload.id,
          count: action.payload.count
        })
    },
    removeProductFromBasket: (state, action) => {
      state.basketList.filter(product => product.id !== action.payload.id)
    },
    removeAllProductFromBasket: (state) => {
      state.basketList = []
    },
    incrementNumberOfProduct: (state, action) => {
      state.basketList.map(product => {
        if (product.id === action.payload.id) {
          const newCount = product.count + action.payload.count
          product.count = newCount
        }
        return product
      })
    },
    decrementNumberOfProduct: (state, action) => {
      state.basketList.map(product => {
        if (product.id === action.payload.id) {
          const newCount = product.count - action.payload.count
          product.count = newCount
        }
        return product
      })
      const deleteProduct = state.basketList.filter(({ count }) => count > 0)
      state.basketList = deleteProduct
    }
  }
})

export const { addProductToBasket, removeProductFromBasket, removeAllProductFromBasket, incrementNumberOfProduct, decrementNumberOfProduct } = basketSlice.actions

export default basketSlice.reducer
