import { createSlice } from '@reduxjs/toolkit'

export const productSlice = createSlice({
  name: 'product',
  initialState: {
    productList: [
      {
        id: 1,
        title: 'Чёткий товар',
        desc: 'Подходи покупай не сы товар чёткий меткий редкий',
        price: 2000
      },
      {
        id: 2,
        title: 'Товар бомбический',
        desc: 'Супер круто супер клас покупай только у нас',
        price: 3000
      },
      {
        id: 3,
        title: 'Мега товар',
        desc: 'Такова больше нигде нет, сам бы купил но у меня он уже есть',
        price: 1000
      }
    ]
  },
  reducers: {
    addProductToStore: (state, action) => {
      state.productList.push({
        id: +new Date(),
        ...action.payload
      })
    },
    removeProductFromStore: (state, action) => {
      const newProductList = state.productList.filter(product => product.id !== action.payload.id)

      state.productList = newProductList
    }
  }
})

export const { addProductToStore, removeProductFromStore } = productSlice.actions

export default productSlice.reducer
