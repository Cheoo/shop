import './App.css'
import Header from './components/header'
import Products from './components/products'

function App () {
  return (
    <div className='app'>
      <Header/>
      <main>
        <section>
          <div className='container'>
            <Products />
          </div>
        </section>
      </main>
    </div>
  )
}

export default App
