export const productListSelector = state => state.products.productList

export const productByIdSelector = (productId) => (state) => productListSelector(state).find(item => item.id === productId)
