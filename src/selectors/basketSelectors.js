export const basketListSelector = state => state.basket.basketList

export const productInBasketByIdSelector = (productId) => (state) => basketListSelector(state).find(item => item.id === productId)
export const numberOfItemsInBasketSelector = (productId) => (state) => productInBasketByIdSelector(productId)(state)?.count
export const isProductInBasketByIdSelector = (productId) => (state) => Boolean(productInBasketByIdSelector(productId)(state))
