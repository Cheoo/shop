import React from 'react'
import Basket from './basket'

export default function Header () {
  return (
        <div className="header">
            <div className="container">
                <div className="header__wrap">
                    <div>Типа интернет магазин</div>
                    <Basket/>
                </div>
            </div>
        </div>
  )
}
