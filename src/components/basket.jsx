import { useSelector } from 'react-redux'
import { basketListSelector } from '../selectors/basketSelectors'
import BasketList from './basketList'

export default function Basket () {
  const basket = useSelector(basketListSelector)

  return (
        <div className='basket'>
            <div className="basket__count"><p>Товаров в корзине:</p>{basket.length}</div>
            <BasketList />
        </div>
  )
}
