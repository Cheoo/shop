import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { numberOfItemsInBasketSelector, isProductInBasketByIdSelector } from '../selectors/basketSelectors'
import { removeProductFromStore } from '../slices/productSlice/productSlice'
import { addProductToBasket, removeProductFromBasket } from '../slices/basketSlice/basketSlice'
import { useCounter } from '../hooks/useCounter'

export default function Product ({ product }) {
  const dispatch = useDispatch()
  const numberOfItemsInBasket = useSelector(numberOfItemsInBasketSelector(product.id))
  const isProductInBasket = useSelector(isProductInBasketByIdSelector(product.id))

  const { plusOneItem, minusOneItem } = useCounter(product)

  const removeProduct = () => {
    dispatch(removeProductFromBasket({ id: product.id }))
    dispatch(removeProductFromStore({ id: product.id }))
  }

  return (
        <div className="product">
            <button className="product__delete" onClick={removeProduct}>X</button>
            <h2 className="product__title">{product.title}</h2>
            <div className="product__desc">{product.desc}</div>
            <div className="product__price">{product.price}руб</div>
            {isProductInBasket
              ? <div><button onClick={plusOneItem}>+</button>{numberOfItemsInBasket}<button onClick={minusOneItem}>-</button></div>
              : <button onClick={() => { dispatch(addProductToBasket({ id: product.id, count: 1 })) }}>Добавить товар в корзину</button>}
        </div>
  )
}
