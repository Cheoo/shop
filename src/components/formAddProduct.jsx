import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { addProductToStore } from '../slices/productSlice/productSlice'

export default function FormAddProduct () {
  const dispatch = useDispatch()

  const productToStore = (form) => {
    const formData = new FormData(form)

    dispatch(addProductToStore({
      title: formData.get('title'),
      desc: formData.get('desc'),
      price: formData.get('price')
    }))

    form.reset()
  }

  const submitForm = useCallback(e => {
    e.preventDefault()
    productToStore(e.target)
  })

  return (
    <form onSubmit={submitForm}>
      <label>
        <h3>Заголовок</h3>
        <input name='title' type="text"/>
      </label>
      <label>
        <h3>Описание</h3>
        <textarea name="desc" cols="30" rows="10"></textarea>
      </label>
      <label>
        <h3>Стоимость</h3>
        <input name='price' type="text"/>
      </label>
      <div className='footer-form'>
        <input type="submit" value="Добавить товар"/>
      </div>
    </form>
  )
}
