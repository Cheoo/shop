import { useSelector, useDispatch } from 'react-redux'
import { numberOfItemsInBasketSelector } from '../selectors/basketSelectors'
import { useCounter } from '../hooks/useCounter'
import { removeProductFromBasket } from '../slices/basketSlice/basketSlice'
import { productByIdSelector } from '../selectors/productSelectors'

export default function BasketProduct (props) {
  const dispatch = useDispatch()
  const product = useSelector(productByIdSelector(props.id))
  const numberOfItemsInBasket = useSelector(numberOfItemsInBasketSelector(props.id))
  const removeProduct = () => dispatch(removeProductFromBasket({ id: product.id }))

  const { plusOneItem, minusOneItem } = useCounter(product)

  return (
        <div className="basket__product">
            <h3>{product.title}</h3>
            <div>{product.desc}</div>
            <div>{product.price * numberOfItemsInBasket}</div>
            <div className="basket__btns">
                <div>
                    <button onClick={plusOneItem}>+</button>
                        {numberOfItemsInBasket}
                    <button onClick={minusOneItem}>-</button>
                </div>
                    <button onClick={removeProduct}>Удалить все</button>
            </div>
        </div>
  )
}
