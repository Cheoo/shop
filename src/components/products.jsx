import React from 'react'
import { useSelector } from 'react-redux'
import FormAddProduct from '../components/formAddProduct'
import { productListSelector } from '../selectors/productSelectors'
import Product from './product'

export default function Products () {
  const products = useSelector(productListSelector)

  return (
        <div className="products">
            {products.map(product => {
              return (
                <Product product={product} key={product.id}/>
              )
            })}
            <FormAddProduct />
        </div>
  )
}
