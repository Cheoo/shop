import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { removeAllProductFromBasket } from '../slices/basketSlice/basketSlice'
import { basketListSelector } from '../selectors/basketSelectors'
import BasketProduct from './basketProduct'

export default function BasketList (props) {
  const basket = useSelector(basketListSelector)
  const dispatch = useDispatch()

  const removeAllProduct = () => {
    dispatch(removeAllProductFromBasket())
  }

  if (!basket.length) {
    return (
        <div className="basket__list">
            Корзина пуста
        </div>
    )
  }

  return (
        <div className="basket__list">
            <div>
                <button onClick={removeAllProduct}>Удалить все товары</button>
                <div>
                    {basket.map(product => <BasketProduct id={product.id} key={product.id}/>)}
                </div>
            </div>
        </div>
  )
}
