import { configureStore, combineReducers } from '@reduxjs/toolkit'
import basketSlice from '../slices/basketSlice/basketSlice'
import productSlice from '../slices/productSlice/productSlice'

const rootReducer = combineReducers({
  basket: basketSlice,
  products: productSlice
})

const localStorageMiddleware = ({ getState }) => {
  return next => action => {
    const result = next(action)
    localStorage.setItem('applicationState', JSON.stringify(getState()))
    return result
  }
}

const reHydrateStore = () => {
  if (localStorage.getItem('applicationState') !== null) {
    return JSON.parse(localStorage.getItem('applicationState'))
  }
}

export const store = configureStore({
  reducer: rootReducer,
  preloadedState: reHydrateStore(),
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(localStorageMiddleware)
})
