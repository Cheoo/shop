import { useDispatch } from 'react-redux'
import { incrementNumberOfProduct, decrementNumberOfProduct } from '../slices/basketSlice/basketSlice'

export const useCounter = (product) => {
  const dispatch = useDispatch()
  const plusOneItem = () => dispatch(incrementNumberOfProduct({ id: product.id, count: 1 }))
  const minusOneItem = () => dispatch(decrementNumberOfProduct({ id: product.id, count: 1 }))

  return {
    plusOneItem,
    minusOneItem
  }
}
